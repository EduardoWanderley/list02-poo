package entities;

public class Owner {
	
	private String name;
	private String cpf;
	private String adress;
	private String phone;

	public Owner(String name, String cpf, String adress, String phone) {
		this.name = name;
		this.cpf = cpf;
		this.adress = adress;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return " " + name + ", cpf = " + cpf + ", endere�o = " + adress + ", telefone =" + phone;
	}

}
