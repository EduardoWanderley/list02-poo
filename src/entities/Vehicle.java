package entities;

public class Vehicle {
	private String vehiclePlate;
	private Integer year;
	private Double value;
	private Owner owner;

	public Vehicle(String placa, Integer year, Double value, Owner owner) {
		this.vehiclePlate = placa;
		this.year = year;
		this.value = value;
		this.owner = owner;
	}

	public String getPlaca() {
		return vehiclePlate;
	}

	public void setPlaca(String placa) {
		this.vehiclePlate = placa;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Placa do ve�culo = " + vehiclePlate + ", ano = " + year + ", valor = " + value + ", Dados do propriet�rio :"
				+ owner;
	}

}
