package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import entities.Owner;
import entities.Vehicle;

public class Program {

	public static void main(String[] args) {
		obterDados();
	}

	public static void obterDados() {
		Scanner in = new Scanner(System.in);
		List<Vehicle> list = new ArrayList<>();
		char res = 's';

		while (res == 's') {

			System.out.println("#DADOS DO PROPRIET�RIO :");

			System.out.println("Digite o nome do propriet�rio :");
			String name = in.nextLine();

			System.out.println("Digite o cpf do propriet�rio :");
			String cpf = in.next();

			System.out.println("Digite o endere�o do propriet�rio :");
			in.nextLine();
			String adress = in.nextLine();

			System.out.println("Digite o telefone do propriet�rio :");
			String phoneNumber = in.nextLine();

			System.out.println("");
			System.out.println("#DADOS DO VE�CULO :");

			System.out.println("Digite a placa do ve�culo :");
			String vehiclePlate = in.next();

			System.out.println("Digite o ano do ve�culo :");
			int year = in.nextInt();

			System.out.println("Digite o valor do ve�culo :");
			double value = in.nextDouble();

			Vehicle vehicle = new Vehicle(vehiclePlate, year, value, new Owner(name, cpf, adress, phoneNumber));
			list.add(vehicle);

			System.out.println("Deseja continuar ? : [s/n]");
			res = in.next().toLowerCase().charAt(0);
			in.nextLine();
		}

		System.out.println("Lista de ve�culos :");
		for (Vehicle v : list) {
			System.out.println(v);
		}

		in.close();
	}
}
